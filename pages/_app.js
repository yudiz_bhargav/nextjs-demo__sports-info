import { useState } from 'react';
import Router from 'next/router';
import styles from '../styles/loading.module.css';
import '../styles/globals.css';
import LoadingSpinner from '../components/LoadingSpinner';

function App({ Component, pageProps }) {
  const [isLoading, setIsLoading] = useState(false);

  Router.onRouteChangeStart = (url) => {
    setIsLoading(true);
  };

  Router.onRouteChangeComplete = (url) => {
    setIsLoading(false);
  };

  return (
    <>
      {isLoading && <LoadingSpinner />}
      {!isLoading && <Component {...pageProps} />}
    </>
  );
}

export default App;
