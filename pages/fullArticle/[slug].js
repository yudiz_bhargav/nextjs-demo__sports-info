import React from 'react';
import Layout from '../../components/Layout';
import Hero from '../../components/FullArticleHero';
import styles from "../../styles/fullArticle.module.css";

export default function FullArticle({ FinalData }) {
  const final_month = new Date().getMonth() - new Date(FinalData.dCreatedAt).getMonth;

  return (
    <>
      <Layout title={FinalData.sTitle}>
        <Hero
          image={FinalData.sImage}
          title={FinalData.sTitle}
          comment={FinalData.nCommentsCount}
          view={FinalData.nViewCounts}
          month={final_month}
        />
        <div className={styles.mainDes}>
          <p className={styles.description}>
            {FinalData.sDescription}
          </p>
        </div>
      </Layout>
    </>
  );
}

export async function getServerSideProps({ query }) {
  const { slug } = query;

  const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/view/${slug}`);
  const { data } = await res.json();
  const FinalData = data;
  return {
    props: {
      FinalData,
    },
  };
}
