import React, { useState, useEffect } from 'react';
import axios from 'axios';
import SportsCard from '../components/SportsCard';
import Layout from '../components/Layout';
import styles from '../styles/loading.module.css';
import LoadingSpinner from '../components/LoadingSpinner';
import LoadMoreBtn from '../components/LoadMoreBtn';

export default function MainPage({ data }) {
  const [count, setCount] = useState(0);
  const [slugData, setSlugData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const fetchData = async () => {
    console.log("called fetchData function");
    setIsLoading(true);
    const limit = { nStart: count, nLimit: 5 };
    console.log("limit object", limit);
    const res = await axios.post(
      `${process.env.NEXT_PUBLIC_API_URL}/recent`,
      limit
    );
    const data = res.data.data;
    setSlugData([...slugData, ...data]);
    setIsLoading(false);
  };

  useEffect(() => {
    count >= 5 && fetchData();
  }, [count]);

  const sportData = data.data;

  let today_month = new Date().getMonth();

  return (
    <section style={{marginTop: "20rem"}}>
      <Layout title="Sports.info">
        {sportData.map((data, index) => {
          let final_month = new Date().getMonth() - new Date(data.dCreatedAt).getMonth();

          return (
            <div key={index}>
              <SportsCard
                image={data.sImage}
                title={data.sTitle}
                slug={data.sSlug}
                description={data.sDescription}
                view={data.nViewCounts}
                month={final_month}
                comment={data.nCommentsCount}
              />
            </div>
          );
        })}

        {slugData?.map((data) => {
          let date = new Date(data.dCreatedAt);
          let month = date.getMonth();
          let final_month = today_month - month;

          return (
            <>
              <SportsCard
                image={data.sImage}
                title={data.sTitle}
                slug={data.sSlug}
                description={data.sDescription}
                view={data.nViewCounts}
                month={final_month}
                comment={data.nCommentsCount}
              />
            </>
          );
        })}
        <div>
          {isLoading && <LoadingSpinner />}
          {!isLoading && <LoadMoreBtn setCount={setCount} count={count} setIsLoading={setIsLoading} />}
        </div>
      </Layout>
    </section>
  );
}

export async function getStaticProps() {
  const limit = { nStart: 0, nLimit: 5 };
  const res = await axios.post(
    `${process.env.NEXT_PUBLIC_API_URL}/recent`,
    limit
  );

  const data = res.data;

  if (!data) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    };
  }

  return {
    props: {
      data,
    },
  };
}
