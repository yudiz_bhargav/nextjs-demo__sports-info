import React from "react";
import Link from "next/link";
import Layout from '../components/Layout';
import styles from "../styles/error.module.css";

export default function Error() {
  return (
    <Layout title="Page not Found">
    <section className="error-page section">
      <div className="error-container">
        <h1>Oops! It's a dead end</h1>
        <Link href="/" className="btn btn-primary">
          Back Home
        </Link>
      </div>
    </section>
    </Layout>
  );
}