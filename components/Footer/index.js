import React from 'react'
import styles from "../../styles/footer.module.css";

const Footer = () => {
  return (
    <div className={styles.footerContainer}>
      <footer className={styles.footer}>
        <div>
          <h6 className={styles.footerText}>&copy;2019 Sports Info. All Rights Reserved.</h6>
        </div>
      </footer>
    </div>
  )
}

export default Footer
