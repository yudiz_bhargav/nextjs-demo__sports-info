import React from 'react'
import styles from "../../styles/loading.module.css"

const LoadingSpinner = () => {
  return (
    <div className={styles.red_loader}>
      <span></span>
    </div>
  )
}

export default LoadingSpinner
