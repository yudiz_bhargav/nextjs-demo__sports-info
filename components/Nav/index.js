import React from 'react'
import Link from 'next/link';
import styles from "../../styles/nav.module.css"

const Navbar = () => {
  return (
    <nav className={styles.nav}>
      <ul className={styles.navItems__container}>
        <li className={`${styles.navItem} ${styles.navLogo}`}>
        <Link href="/">
          <img src={"https://www.sports.info/assets/images/logo_v2.svg"} alt="Sports Info Logo" />
          </Link>
        </li>
        <li className={`${styles.navItem} ${styles.navSearch__icon}`}>
          <img src={"https://www.sports.info/search-icon.1877dcb81fe5eb939969.svg"} alt="Search" />
        </li>
      </ul>
    </nav>
  )
}

export default Navbar
