import React from 'react'
import styles from "../../styles/loadMoreBtn.module.css"

const LoadMoreBtn = ({ setCount, count, setIsLoading }) => {
  return (
    <div className={styles.loadMore__btnMainContainer} onClick={() => {
      setCount(count + 5); 
      setIsLoading(true);
    }
    }>
      <div className={styles.loadMore__btnContainer}>
        <button className={styles.loadMore__btn}>Load More</button>
        <span className={styles.loadMore__downArrowContainer}>
          <img className={`${styles.loadMore__downArrow} ${styles.firstDownArrow}`} src={"https://www.sports.info/right-arrow.318a2f1f7fc52ec4ef47.svg"} alt="down-arrow" />
          <img className={styles.loadMore__downArrow} src={"https://www.sports.info/right-arrow.318a2f1f7fc52ec4ef47.svg"} alt="down-arrow" />
        </span>
      </div>
    </div>
  )
}

export default LoadMoreBtn
