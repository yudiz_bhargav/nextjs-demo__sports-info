import React from 'react';
import styles from "../../styles/sportsCards.module.css";
import Link from 'next/link';

const SportsCard = ({ image, slug, title, description, month, comment, view }) => {
  return (
    <article className={styles.sportsCard__container}>
      <div className={styles.sportsCard__imageContainer}>
        <img className={styles.sportsCard__image} src={image} alt="Sports Card Image" />
      </div>
      <div className={styles.sportsCard__bodyContainer}>
        <div className={styles.sportsCard__bodyTextContainer}>
          <Link href="/fullArticle/[slug]" as={'/fullArticle/' + slug}>
            <h3>{title}</h3>
          </Link>
          <p>{description}</p>
        </div>
        <div className={styles.sportsCard__bodyFooterContainer}>
          <div className={styles.sportsCard__bodyFooterLinkMonthContainer}>
            <a className={styles.sportsCard__bodyFooterLink} href="#">SPORTS INFO</a>
            <span>{month} months ago</span>
          </div>
          <div className={styles.sportsCard__bodyFooterCommentViewContainer}>
            <img className={styles.sportsCard__bodyFooterCommentIcon} src={"https://www.sports.info/comment-icon.7aef209a3b2086028430.svg"} alt="comments" />
            <span className={styles.sportsCard__bodyFooterCommentSpan}>{comment}</span>
            <img className={styles.sportsCard__bodyFooterEyeIcon} src={"https://www.sports.info/view-icon.b16661e96527947b18f1.svg"} alt="views" />
            <span className={styles.sportsCard__bodyFooterEyeSpan}>{view}</span>
          </div>
        </div>
      </div>
    </article>
  );
};

export default SportsCard;
