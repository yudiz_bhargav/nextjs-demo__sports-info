import React from 'react'
import styles from '../../styles/sportsInfo.module.css';

const ArticleHero = ({ image, title, month, comment, view }) => {
  // TODO do something about the image, instead of importing it in CSS, import it here
  return (
    <header className={styles.sportInfoHeader}>
      <div className={styles.sportInfoHeader__bodyContainer}>
        <h2 className={styles.sportInfoTitle}>{title}</h2>
        <div className={styles.sportInfoHeader__footerContainer}>
          <div className={styles.sportInfoHeader__footerLinkMonthContainer}>
            <a className={styles.sportInfoHeader__footerLink} href="#">SPORTS INFO</a>
            <span>{month} months ago</span>
          </div>
          <div className={styles.sportInfoHeader__footerCommentViewContainer}>
            <img className={styles.sportInfoHeader__footerCommentIcon} src={"https://www.sports.info/comment-icon.7aef209a3b2086028430.svg"} alt="comments" />
            <span className={styles.sportInfoHeader__footerCommentSpan}>{comment}</span>
            <img className={styles.sportInfoHeader__footerEyeIcon} src={"https://www.sports.info/view-icon.b16661e96527947b18f1.svg"} alt="views" />
            <span className={styles.sportInfoHeader__footerEyeSpan}>{view}</span>
          </div>
        </div>
      </div>
    </header>
  )
}

export default ArticleHero
